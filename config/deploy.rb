set :repository, "git@github.com:Chongsawad/radiant.git"

set :user, ENV['USER'] || 'inice'
set :application, ENV['APPNAME'] || 'inice_radiant'
set :domain, 'inice@inice'
#set :ssh_options, {:forward_agent => true}
set :scm, :git

set :deploy_to, "/home/inice/users/#{user}/#{application}"
set :password, "kckctv9t2925"

role :app, domain 
role :web, domain 
role :db,  domain , :primary => true

require 'erb'

after "deploy:setup", "deploy:restart"
after "deploy:restart", "database:config"
after "database:config", "database:create"
after "database:create", "database:setup"

namespace :deploy do
  task :restart, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
    run "mkdir -p #{shared_path}/log"
    run "mkdir -p #{shared_path}/pids"
    run "mkdir -p #{shared_path}/system"
  end

end

namespace :database do

  task :config do
    puts "------------- Generate Databaseyml ---------------"
    db_config = ERB.new <<-EOF
base: &base
  adapter: mysql
  encoding: utf8
  reconnect: false
  pool: 5
  username: root
  password:
  host: localhost

development:
  database: #{application}_development
  <<: *base

test:
  database: #{application}_test
  <<: *base

production:
  database: #{application}_production
  <<: *base
EOF
    run "mkdir -p #{current_path}/config/"
    put db_config.result, "#{current_path}/config/database.yml"
  end

  task :create do
    puts "------------- Create Database ---------------"
    rake = fetch(:rake,"rake")
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_release}; #{rake} RAILS_ENV=#{rails_env} db:create"
  end

  task :drop do
    puts "------------- Drop Database ---------------"
    rake = fetch(:rake,"rake")
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_release}; #{rake} RAILS_ENV=#{rails_env} db:drop"
  end
  
  task :remigrate do
    puts "------------- Remigrate Database ---------------"
    rake = fetch(:rake,"rake")
    rails_env = fetch(:rails_env, "production")
    #run "cd #{current_release}; #{rake} RAILS_ENV=#{rails_env} db:drop"
  end

  after "database:remigrate","database:drop"
  after "database:drop","database:create"

  task :setup do
    rake = fetch(:rake,"rake")
    rails_env = fetch(:rails_env, "production")
    run "cd #{current_release}" 
    puts "------------- Bootstrap ---------------"
    run "cd #{current_release}; #{rake} RAILS_ENV=#{rails_env} ADMIN_NAME=Administrator ADMIN_USERNAME=admin ADMIN_PASSWORD=admin DATABASE_TEMPLATE=roasters.yml OVERWRITE=true db:bootstrap"
  end
end

